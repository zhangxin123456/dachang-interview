# 大厂面试题第一季

## 1. JavaSE面试题

### 1.1 自增变量

- 代码的执行结果是什么呢？

```java
public static void main(String[] args) {
        int i = 1;
        i = i++;
        int j = i++;
        int k = i + ++i * i++;
        System.out.println("i = " + i);
        System.out.println("j = " + j);
        System.out.println("k = " + k);
    }
```

1）i = i++; 字节码解析

会先执行 = 号右边的，将 i 压入栈中，操作数栈为 1，然后 i 自增，局部变量表中 i 的值会从 1 变成 2，等号右边操作完成，然后是赋值操作，将操作数栈结果赋值 i 变量，则 i 变量的值为 1。

![image-20220217225135144](./images/image-20220217225135144.png)

2）int j = i++; 字节码解析

首先执行等号右边操作，将 i 压入操作数栈中值为 1，然后 i 进行自增操作，局部变量表 i 的值就为 2，执行赋值操作，将操作数栈的 1 赋值给 j ，则局部变量表中 j 的值就为 1。

![image-20220217225221086](./images/image-20220217225221086.png)

3）int k = i + ++i * i++; 字节码解析

首先执行等号右边的操作，将 i 压入到操作数栈中值为 2，然后先执行乘法操作，++i 操作是先自增，在局部变量表中，i 的值就为 3，然后将 i 压入到数据栈中值为 3 ，i++ 操作将局部变量表中 i 的值压入到操作数栈中值为3，然后执行自增操作，变量表中 i 的值就为 4，从数据栈中 pop 出 2 个最后压入栈的值进行乘法操作 3 * 3 = 9，然后 push 到栈中，此时栈中的值有两个，一个的最开始压入栈的 2 和 刚刚 push 栈的 9，从栈中取出两个数执行加法操作，结果为 11 ，然后压入栈中赋值给变量表中的 k，此时 k 的值为 11。
![image-20220217225300092](./images/image-20220217225300092.png)

#### 总结：

- 1）赋值 =，最后计算
- 2）= 右边的从左到右加载值依次压入操作数栈
- 3）实际先算哪个，看运算符优先级
- 4）自增、自减操作都是直接修改变量的值，不经过操作数栈
- 5）最后赋值之前，临时结果也是存储在操作数栈**

#### 拓展

-  i = i++;

```java
public static void main(String[] args) {
    int i = 1;
    i = i++;
    System.out.println(i);
}

 0: iconst_1 
 1: istore_1 // 将 1 存入 局部变量表
 2: iload_1  // 将 1 加载到  操作数栈
 3: iinc          1, 1  // 局部变量表中1自增操作
 6: istore_1            //   操作数栈顶元素加载到局部变量表第一位（会将局部局部变量表中2更新为1）
 7: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
10: iload_1        
11: invokevirtual #3                  // Method java/io/PrintStream.println:(I)V
14: return
```



-   i = ++i;

```java
public static void main(String[] args) {
    int i = 1;
    i = ++i;
    System.out.println(i);
}
 0: iconst_1
 1: istore_1      // 将 1 存入 局部变量表
 2: iinc          1, 1   // 局部变量表中1自增操作
 5: iload_1       // 将局部变量表第一位（数字2） 加载到  操作数栈
 6: istore_1      //   操作数栈顶元素 2 加载到局部变量表第一位
 7: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
10: iload_1
11: invokevirtual #3                  // Method java/io/PrintStream.println:(I)V
14: return
```

-   i = i+1;

```java
public static void main(String[] args) {
    int i = 1;
    i = i+1;
    System.out.println(i);
}

0: iconst_1
1: istore_1  // 将 1 存入 局部变量表
2: iload_1     // 将局部变量表第一位（数字2） 加载到  操作数栈
3: iconst_1    //常量1 加入  操作数栈
4: iadd      //执行相加操作 
5: istore_1    //   操作数栈顶元素 2 加载到局部变量表第一位
6: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
9: iload_1
10: invokevirtual #3                  // Method java/io/PrintStream.println:(I)V
13: return

```

### 1.2 单例设计模式


#### 什么是 Singleton ？

- Singleton：在Java中即指单例设计模式，它是软件开发中最常用的设计模式之一。
- 单：唯一
- 例：实例
- 单例设计模式，即某个类在整个系统中只能有一个实例对象可被获取和使用的代码模式。
- 例如：代表JVM运行环境的Runtime类

#### 要点

- 某个类只能有一个实例（构造器私有化）
- 它必须自行创建实例（ 含有一个该类的静态变量来保存这个唯一的实例）
- 它必须自行向整个系统提供这个实例（对外提供获取该类实例对象的方式直接暴露，用静态变量的get方法获取）

#### 常见的形式

饿汉式：直接创建对象，不存在线程安全问题

- 直接实例化饿汉式（简洁直观）
- 枚举式（最简洁）
- 静态代码块饿汉式（适合复杂实例化）


懒汉式：延迟创建对象

- 线程不安全（适用于单线程）
- 线程安全（适用于多线程）
- 静态内部类形式（适用于多线程）



##### 1）直接实例化饿汉式（简洁直观）

```java
/**
 * 直接实例化饿汉式
 */
public class Code_02_Singleton1 {

    /**
     * 1、构造器私有化
     * 2、自行创建，并且用静态变量保存
     * 3、向外提供实例
     * 4、强调这是一个单例，我们可以用final修改
     */
    public static final Code_02_Singleton1 INSTANCE = new Code_02_Singleton1();

    private Code_02_Singleton1() {

    }

}

```

##### 2） 静态代码块饿汉式（适合复杂实例化）

```java
/**
 * 静态代码块饿汉式(适合复杂实例化)
 */
public class Code_02_Singleton2 {

    public static final Code_02_Singleton2 INSTANCE;

    static {
        INSTANCE = new Code_02_Singleton2();
    }

    private Code_02_Singleton2() {
        
    }

}

```

##### 3）枚举饿汉式 (最简洁)

```java
/**
 * 枚举式 (最简洁)
 */
public enum Code_02_Singleton3 {
    /**
     * 枚举类型：表示该类型是有限的几个
     */
    INSTANCE
}

```

##### 4）线程不安全懒汉式(适用于单线程)

```java
/**
 * 线程不安全(使用于单线程)
 */
public class Code_02_Singleton4 {

    /**
     * 1、构造器私有化
     * 2、用一个静态变量保存这个唯一的实例
     * 3、提供一个静态方法，获取这个实例对象
     */
    public static Code_02_Singleton4 instance;

    private Code_02_Singleton4() {

    }

    public static Code_02_Singleton4 getInstance() {

        if(instance == null) {
            instance = new Code_02_Singleton4();
        }
        
        return instance;
    }

}

```

##### 5）双重检查懒汉式(线程安全，适用于多线程)

```java
/**
 * 双重检查(线程安全，适用于多线程)
 */
public class Code_02_Singleton5 {

    // 加 volatile 作用：防止指令重排, 当实例变量有修改时，能刷到主存中去是一个原子操作，并且保证可见性。
    public static volatile Code_02_Singleton5 instance;

    private Code_02_Singleton5() {

    }

    public static Code_02_Singleton5 getInstance() {
        if(instance == null) {
            synchronized (Code_02_Singleton5.class) {
                if(instance == null) {
                    instance = new Code_02_Singleton5();
                }
            }

        }
        return instance;
    }

}


```

##### 6）静态内部类模式 (适用于多线程)

```java
/**
 * 静态内部类模式 (适用于多线程)
 */
public class Code_02_Singleton6 {

    /**
     * 1、内部类被加载和初始化时，才创建INSTANCE实例对象
     * 2、静态内部类不会自动创建, 不会随着外部类的加载初始化而初始化，他是要单独去加载和实例化的
     * 3、因为是在内部类加载和初始化时，创建的，因此线程安全
     */
    public static class Inner {
        private static final Code_02_Singleton6 INSTANCE = new Code_02_Singleton6();
    }

    private Code_02_Singleton6() {

    }

    public static Code_02_Singleton6 getInstance() {
        return Inner.INSTANCE;
    }
}
s
```

#### 总结：

- 1、如果是饿汉式，枚举形式最简单
- 2、如果是懒汉式，静态内部类形式最简单



### 1.3 初始化和实例初始化

- 首先看一道题目如下：

![image-20220217230545527](./images/image-20220217230545527.png)



代码如下：

```java
/**
 * 父类初始化 <clinit>
 * 1、j = method()
 * 2、父类的静态代码块
 *
 * 父类实例化方法:
 * 1、super()（最前）
 * 2、i = test() (9)
 * 3、子类的非静态代码块 (3)
 * 4、子类的无参构造（最后）(2)
 *
 *
 * 非静态方法前面其实有一个默认的对象this
 * this在构造器或 <init> 他表示的是正在创建的对象，因为咱们这里是正在创建Son对象，所以
 * test()执行的就是子类重写的代码(面向对象多态)
 *
 * 这里i=test() 执行的就是子类重写的test()方法
 */
public class Code_03_Father {

    private int i = test();
    private static int j = method();

    static {
        System.out.print("(1)");
    }

    public Code_03_Father() {
        System.out.print("(2)");
    }

    {
        System.out.print("(3)");
    }

    public int test() {
        System.out.print("(4)");
        return 1;
    }

    public static int method() {
        System.out.print("(5)");
        return 1;
    }

}


```



```java
/**
 * 子类的初始化 <clinit>
 * 1、j = method()
 * 2、子类的静态代码块
 *
 * 先初始化父类 (5)(1)
 * 后初始化子类 (10) (6)
 *
 * 子类实例化方法:
 * 1、super()（最前）
 * 2、i = test() (9)
 * 3、子类的非静态代码块 (8)
 * 4、子类的无参构造（最后） (7)
 */
public class Code_03_Son extends Code_03_Father{

    private int i = test();
    private static int j = method();

    static {
        System.out.print("(6)");
    }

    public Code_03_Son() {
        System.out.print("(7)");
    }

    {
        System.out.print("(8)");
    }

    public int test() {
        System.out.print("(9)");
        return 1;
    }
    public static int method() {
        System.out.print("(10)");
        return 1;
    }

    public static void main(String[] args) {
        Code_03_Son s1 = new Code_03_Son(); // 5 1 10 6 9 3 2 9 8 7
        System.out.println();
        Code_03_Son s2 = new Code_03_Son(); // 9 3 2 9 8 7
    }

}

```

#### 考点

- 类初始化过程
- 实例初始化过程
- 方法的重写

#### 类初始化过程

- 一个类要创建实例需要先加载并初始化该类
  - main方法所在的类需要先加载和初始化
- 一个子类要初始化需要先初始化父类
- 一个类初始化就是执行<clinit>()方法
  - <clinit>()方法由静态类变量显示赋值代码和静态代码块组成
  - 类变量显示赋值代码和静态代码块代码从上到下顺序执行
  - <clinit>()方法只执行一次



#### 实例初始化过程

**实例初始化就是执行<init>()方法**

- <init>()方法可能重载有多个，有几个构造器就有几个<init>方法
- <init>()方法由非静态实例变量显示赋值代码和非静态代码块、对应构造器代码组成
- 非静态实例变量显示赋值代码和非静态代码块代码从上到下顺序执行，而对应构造器的代码最后执行
- 每次创建实例对象，调用对应构造器，执行的就是对应的<init>方法
- <init>方法的首行是super()或super(实参列表)，即对应父类的<init>方法

#### 方法的重写
1）那些方法不可以被重写

- final 声明的方法
- static 声明的方法
- private 等子类中不可见的方法

2）对象的多态性

- 子类如果重写了父类的方法，通过子类对象调用的一定是子类重写过的代码
- 非静态方法默认的调用对象是 this
- this 对象在构造器或者说 <init>() 方法中就是正在创建的对象
  

结果如下：

```java
(5)(1)(10)(6)(9)(3)(2)(9)(8)(7)
(9)(3)(2)(9)(8)(7)
```

### 1.4 方法的参数传递机制

- 首先看一道题目如下：

```java
/**
 * 方法的参数传递机制
 */
public class Code_04_ParameterPassing {

    public static void main(String[] args) {
        int i = 1;
        String str = "hello";
        Integer num = 200;
        int[] arr = {1, 2, 3, 4, 5};
        MyData myData = new MyData();

        change(i, str, num, arr, myData);

        System.out.println("i = " + i); // 1
        System.out.println("str = " + str); // hello
        System.out.println("num = " + num); // 200
        System.out.println("arr = " + Arrays.toString(arr)); // 2, 2, 3, 4, 5
        System.out.println("my.a = " + myData.a); // 11
    }

    public static void change(int j, String s, Integer n, int[] a, MyData m) {
        j += 1;
        s += "world";
        n += 1;
        a[0] += 1;
        m.a += 1;
    }

}

cass MyData {
    int a = 10;
}
```

- 结果如下：

```java
i = 1
str = hello
num = 200
arr = [2, 2, 3, 4, 5]
my.a = 11
```

- 分析

![image-20220218195438538](./images/image-20220218195438538.png)



**形参是基本数据类型**

- 传递数据值

**实参是引用数据类型**

- 传递地址值
- 特殊的类型：String、包装类等对象不可变性

#### 考点

1）方法的参数传递机制
2）String、包装类等对象的不可变性

### 1.5 迭代与递归

- 编程题：有n步台阶，一次只能上1步或2步，共有多少种走法？

#### 递归

- n=1      ->一步 ->f(1) = 1
- n=2      ->(1)一步一步(2)直接2步->f(2) = 2 
- n=3      ->(1)先到达f(1)，然后从f(1)直接跨2步            (2)先到达f(2)，然后从f(2)跨1步 ->f(3) = f(1) + f(2)
- n=4      ->(1)先到达f(2)，然后从f(2)直接跨2步            (2)先到达f(3)，然后从f(3)跨1步->f(4) = f(2) + f(3)
- ....
- n=x        ->(1)先到达f(x-2)，然后从f(x-2)直接跨2步    (2)先到达f(x-1)，然后从f(x-1)跨1步    ->f(x) = f(x-2) + f(x-1)                

**代码**

```java
/**
 * 编程题：有 n 步台阶，一次只能上 1 步或者 2 步，共有多少种走法
 */
public class Code_05_StepProblem {

    @Test
    public void test() {
        // 时间复杂度 ...
        long start = System.currentTimeMillis();
        System.out.println(recursion(40)); // 165580141
        long end = System.currentTimeMillis(); // 537
        System.out.println(end - start);


    }

    // 递归实现
    public int recursion(int n) {
      if(n < 1) {
          return 0;
      }
      if(n == 1 || n == 2) {
          return n;
      }
      return recursion(n - 2) + recursion( n - 1);
    }


}

```



#### 循环迭代

- n=1       ->一步->f(1) = 1
- n=2       ->(1)一步一步 (2)直接2步     ->f(2) = 2 
- n=3       ->(1)先到达f(1)，然后从f(1)直接跨2步 (2)先到达f(2)，然后从f(2)跨1步     ->f(3) = two + one f(3) = f(1) + f(2) two = f(1) ; one = f(2)
- n=4      ->(1)先到达f(2)，然后从f(2)直接跨2步(2)先到达f(3)，然后从f(3)跨1步     ->f(4) = two + onef(4) = f(2) + f(3) two = f(2);    one = f(3)    
- ....
- n=x      ->(1)先到达f(x-2)，然后从f(x-2)直接跨2步   (2)先到达f(x-1)，然后从f(x-1)跨1步        ->f(x) = two + onef(x) = f(x-2) + f(x-1) two = f(x-2); one = f(x-1)        



**代码**

```java
/**
 * 编程题：有 n 步台阶，一次只能上 1 步或者 2 步，共有多少种走法
 */
public class Code_05_StepProblem {

    @Test
    public void test() {
        // 时间复杂度 O(n)
        long start = System.currentTimeMillis();
        System.out.println(iteration(40)); // 165580141
        long end = System.currentTimeMillis(); // 0
        System.out.println(end - start);
    }
    // 迭代实现
    public int iteration(int n) {
        if(n < 1) {
            return 0;
        }
        if(n == 1 || n == 2) {
            return n;
        }
        int two = 1; // 一层台阶，有 1 走法, n 的前两层台阶的走法
        int one = 2; // 二层台阶，有 2 走法, n 的前一层台阶的走法
        int sum = 0; // 记录一共有多少中走法
        for(int i = 3; i <= n; i++) {
                sum = two + one;
                two = one;
                one = sum;
        }
        return sum;
    }
}

```



#### 小结

- 方法调用自身称为递归，利用变量的原值推出新值称为迭代。
- 递归
  - 优点：大问题转化为小问题，可以减少代码量，同时代码精简，可读性好；
  - 缺点：递归调用浪费了空间，而且递归太深容易造成堆栈的溢出。
- 迭代
  - 优点：代码运行效率好，因为时间只因循环次数增加而增加，而且没有额外的空间开销；
  - 缺点：代码不如递归简洁，可读性好

### 1.6 成员变量和局部变量

- 首先看一道题目如下：

```java
/**
 * 成员变量和局部变量
 */
public class Code_06_LocalAndMemberVariable {

    public static int s;
    int i;
    int j;

    {
        int i = 1;
        i++;
        j++;
        s++;
    }

    public void test(int j) {
        j++;
        i++;
        s++;
    }

    public static void main(String[] args) {
        Code_06_LocalAndMemberVariable obj1 = new Code_06_LocalAndMemberVariable();
        Code_06_LocalAndMemberVariable obj2 = new Code_06_LocalAndMemberVariable();

        obj1.test(10);
        obj1.test(20);
        obj2.test(30);

        System.out.println(obj1.i + "," + obj1.j + "," + obj1.s); // 2 1 5
        System.out.println(obj2.i + "," + obj2.j + "," + obj2.s); // 1 1 5
    }

}

```

#### 考点

1）就近原则
2）变量的分类

- 成员变量：类变量、实例变量
- 局部变量

3）非静态代码块的执行：每次创建实例对象都会执行
4）方法的调用规则：调用一次执行一次

#### 局部变量与成员变量的区别

**1）声明的位置**

- 局部变量：方法体 { } 中、代码块 { } 中、形参
- 成员变量：类中的方法外
- 类变量 ：有 static 修饰
- 实例变量：没有 static 修饰

**2）修饰符**

- 局部变量：final
- 成员变量：public、protected、private、final、static、volatile、transient

**3）值存储的位置**

- 局部变量：栈
- 实例变量：堆
- 类变量：方法区

**4）作用域**

- 局部变量：声明处开始，到所属的 } 结束
- 实例变量：在当前类中 “this ”（有时this. 可以省略），在其他类中 “对象名. ” 访问
- 类变量：在当前类中 “类名” （有时类名. 可以省略），在其它类中 “类名.” 或 “对象名.” 访问

**5）生命周期**

- 局部变量：每一个线程，每一次调用执行都是新的生命周期
- 实例变量：随着对象的创建而初始化，随着对象的被回收而消亡，每一个对象的实例变量都是独立的
- 类变量：随着类的初始化而初始化，随着类的卸载而消亡，该类的所有对象的类变量是共享的

#### 当局部变量与XX变量重名时，如何区分

- 1）局部变量与实例变量重名
  - 在实例变量前面加 “this.”

- 2）局部变量与类变量重名
  -  在类变量前面加 “类名.”



## 2. SSM 面试题

### 1. Spring Bean 的作用域之间有什么区别？

**在 Spring 的配置文件中，给 bean 加上 scope 属性来指定 bean 的作用域如下：**

- singleton：唯一 bean 实例，Spring 中的 bean 默认都是单例的。
- prototype：每次请求都会创建一个新的 bean 实例。
- request： 每一次 HTTP 请求都会产生一个新的 bean，该 bean 仅在当前 HTTP request 内有效。
- session：每一次 HTTP 请求都会产生一个新的 bean，该 bean 仅在当前 HTTP session 内有效。
- global-session：全局session作用域，仅仅在基于portlet的web应用中才有意义，Spring5已经没有了。Portlet是能够生成语义代码(例如：HTML)片段的小型Java Web插件。它们基于portlet容器，可以像servlet一样处理HTTP请求。但是，与 servlet 不同，每个 portlet 都有不同的会话。

### 2. Spring 支持的常用数据库事务传播行为和事务的隔离级别？

#### 事务的隔离级别
**TransactionDefinition 接口中定义了五个表示隔离级别的常量：**

- TransactionDefinition.ISOLATION_DEFAULT: 使用后端数据库默认的隔离级别，Mysql 默认采用的 REPEATABLE_READ隔离级别 Oracle 默认采用的 READ_COMMITTED隔离级别.
- TransactionDefinition.ISOLATION_READ_UNCOMMITTED: 最低的隔离级别，允许读取尚未提交的数据变更，可能会导致脏读、幻读或不可重复读
- TransactionDefinition.ISOLATION_READ_COMMITTED: 允许读取并发事务已经提交的数据，可以阻止脏读，但是幻读或不可重复读仍有可能发生
- TransactionDefinition.ISOLATION_REPEATABLE_READ: 对同一字段的多次读取结果都是一致的，除非数据是被本身事务自己所修改，可以阻止脏读和不可重复读，但幻读仍有可能发生。
- TransactionDefinition.ISOLATION_SERIALIZABLE: 最高的隔离级别，完全服从ACID的隔离级别。所有的事务依次逐个执行，这样事务之间就完全不可能产生干扰，也就是说，该级别可以防止脏读、不可重复读以及幻读。但是这将严重影响程序的性能。通常情况下也不会用到该级别。

#### 事务的传播行为

**支持当前事务的情况：**

- TransactionDefinition.PROPAGATION_REQUIRED： 如果当前存在事务，则加入该事务；如果当前没有事务，则创建一个新的事务。
- TransactionDefinition.PROPAGATION_SUPPORTS： 如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务的方式继续运行。
- TransactionDefinition.PROPAGATION_MANDATORY： 如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常。（mandatory：强制性）

**不支持当前事务的情况：**

- TransactionDefinition.PROPAGATION_REQUIRES_NEW： 创建一个新的事务，如果当前存在事务，则把当前事务挂起。
- TransactionDefinition.PROPAGATION_NOT_SUPPORTED： 以非事务方式运行，如果当前存在事务，则把当前事务挂起。
- TransactionDefinition.PROPAGATION_NEVER： 以非事务方式运行，如果当前存在事务，则抛出异常。

**其他情况：**

- TransactionDefinition.PROPAGATION_NESTED： 如果当前存在事务，则创建一个事务作为当前事务的嵌套事务来运行；如果当前没有事务，则该取值等价于TransactionDefinition.PROPAGATION_REQUIRED。

### 3. Spring MVC 如果解决 POST 请求中文乱码问题？

#### 解决 POST 请求中文乱码问题

- 修改项目中web.xml文件

```xml-dtd
  <filter>
    <filter-name>CharacterEncodingFilter</filter-name>
    <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
    <init-param>
      <param-name>encoding</param-name>
      <param-value>UTF-8</param-value>
    </init-param>
    <init-param>
      <param-name>forceEncoding</param-name>
      <param-value>true</param-value>
    </init-param>
  </filter>
  <filter-mapping>
    <filter-name>CharacterEncodingFilter</filter-name>
    <url-pattern>/*</url-pattern>
  </filter-mapping>
```

#### 解决 Get 请求中文乱码问题
- 修改tomcat中server.xml文件

```xml
<Connector URIEncoding="UTF-8" port="8080" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />

```



### 4. Spring MVC 的工作流程？

![image-20220218204110794](./images/image-20220218204110794.png)

![image-20220218204228958](./images/image-20220218204228958.png)

**流程说明（重要）：**

1、客户端（浏览器）发送请求，直接请求到 DispatcherServlet。

2、DispatcherServlet 根据请求信息调用 HandlerMapping，解析请求对应的 Handler。

3、解析到对应的 Handler（也就是我们平常说的 Controller 控制器）后，开始由 HandlerAdapter 适配器处理。

4、HandlerAdapter 会根据 Handler 来调用真正的处理器来处理请求，并处理相应的业务逻辑。

5、处理器处理完业务后，会返回一个 ModelAndView 对象，Model 是返回的数据对象，View 是个逻辑上的 View。

6、ViewResolver 会根据逻辑 View 查找实际的 View。

7、DispaterServlet 把返回的 Model 传给 View（视图渲染）。

8、把 View 返回给请求者（浏览器）

### 5. Mybatis 中当实体类中的属性名和表中的字段不一样，怎么解决？

**解决方案有三种如下：**

1、写 SQL 语句的时候 写别名

2、在MyBatis的全局配置文件中开启驼峰命名规则，前提是符合驼峰命名规则

```xml
<!-- 开启驼峰命名规则，可以将数据库中下划线映射为驼峰命名
	列如 last_name 可以映射为 lastName
-->
<setting name="mapUnderscoreToCameLCase" value="true" />
```

3、在Mapper映射文件中使用 resultMap 自定义映射

```xml
<!-- 
	自定义映射
-->
<resultMap type="com.atguigu.pojo.Employee" id="myMap">
    <!-- 映射主键 -->
	<id cloumn="id" property="id"/>
    <!-- 映射其他列 -->
    <result column="last_name" property="lastName" />
    <result column="email" property="email" />
    <result column="salary" property="salary" />
    <result column="dept_id" property="deptId" />
</resultMap>
```



## 3. Java 高级

### Linux 常用服务类相关命令？

#### 1、centos 6

```shell
service 服务名 start
service 服务名 stop
service 服务名 restart
service 服务名 reload
service 服务名 status

#查看服务的方法 /etc/init.d/ 服务名
#通过 chkconfig 命令设置自启动
#查看服务 chkconfig -list | grep XXX

chkconfig -level 5 服务名 on/off

```

#### 运行级别（centos6）



![img](./images/20210122115557634.png)



**Linux 系统有 7 种运行级别 (runlevel) : 常用的是级别 3 和 5 。**

- 运行级别0: 系统停机状态，系统默认运行级别不能设为0，否则不能正常启动
- 运行级别1: 单用户工作状态，root权限，用于系统维护，禁止远程登陆
- 运行级别2: 多用户状态(没有NFS),不支持网络
- 运行级别3: 完全的多用户状态(有NFS),登陆后进入控制台命令行模式
- 运行级别4: 系统未使用，保留
- 运行级别5: X11控制台，登陆后进入图形GUI模式
- 运行级别6: 系统正常关闭并重启，默认运行级别不能设为6,否则不能正常启动.
  

#### 3、centos 7

```shell
systemctl start 服务名(xxx.service)
systemct restart 服务名(xxxx.service)
systemctl stop 服务名(xxxx.service)
systemctl reload 服务名(xxxx.service)
systemctl status 服务名(xxxx.service)

#查看服务的方法 /usr/lib/systemd/system
#查看服务的命令

systemctl list-unit-files
systemctl --type service

#通过systemctl命令设置自启动

自启动 systemctl enable service_name
不自启动 systemctl disable service_name

```

### Git 分支相关命令?

#### 1、创建分支

> git branch <分支名>
>
> git branch -v 查看分支

#### 2、切换分支

> git checkout <分支名>
>
> 一步完成: git checkout -b <分支名> 

#### 3、合并分支

> 先切换到主干 git checkout master
>
> git merge <分支名>

#### 4、删除分支

> 先切换到主干 git checkout master
>
> git branch -D <分支名>



### redis 持久化有几种类型，它们的区别是？

- **Redis 提供了两种不同形式的持久化的方式。**

#### RDB（Redis DataBase）

**1）什么是 RDB 呢？**

指定时间间隔从内存中的数据集快照写入磁盘，也就是行话讲的 Snapshot 快照，它的恢复是将快照文件读取到内存中。

**2）RDB 备份是如何执行的？**

Redis会单独创建（fork）一个子进程来进行持久化，会先将数据写入到一个临时文件中，待持久化过程都结束了，再用这个临时文件替换上次持久化好的文件。整个过程中，主进程是不进行任何IO操作的，这就确保了极高的性能如果需要进行大规模数据的恢复，且对于数据恢复的完整性不是非常敏感，那RDB方式要比AOF方式更加的高效。RDB的缺点是最后一次持久化后的数据可能丢失。

**3）什么是 fork ？**

在Linux程序中，fork()会产生一个和父进程完全相同的子进程，但子进程在此后多会exec系统调用，出于效率考虑，Linux中引入了“写时复制技术”，一般情况父进程和子进程会共用同一段物理内存，只有进程空间的各段的内容要发生变化时，才会将父进程的内容复制一份给子进程。

**4）RDB 保存的文件**

在 redis.conf 的配置文件中，默认保存文件的名称叫 dump.rdb
![img](./images/20210122124814403.png)

rdb文件的保存路径，也可以修改。默认为Redis启动时命令行所在的目录下

**5）RDB 保存的策略**

![image-20220218212722369](./images/image-20220218212722369.png)

15 分钟 1 次添加 key 的操作，5 分钟 10 次添加 key 的操作，1 分钟 10000 次添加 key 的操作都会触发保存策略。

**6）RDB 的备份**

- 先通过 config get dir 查询 rdb文件的目录
- 将 *.rdb 的文件拷贝到别的地方

**7）RDB 的恢复**

- 关闭 Redis
- 先把备份文件拷贝到拷贝到工作目录下
- 启动 Redis，备份数据会直接加载

**8）RDB 的优点**

- 节省磁盘空间
- 恢复速度快

![img](./images/20210122125453530.png)



**9）RDB 的缺点**

- 虽然Redis在fork时使用了写时拷贝技术,但是如果数据庞大时还是比较消耗性能。
- 在备份周期在一定间隔时间做一次备份, 所以如果Redis意外down掉的话，就会丢失最后一次快照后的所有修改。

#### AOF（Append Only File）

**1）什么是 AOF 呢？**

以日志的形式来记录每个写操作，将Redis执行过的所有写指令记录下来(读操作不记录)，只许追加文件但不可以改写文件，Redis启动之初会读取该文件重新构建数据，换言之，Redis重启的话就根据日志文件的内容将写指令从前到后执行一次以完成数据的恢复工作。

**2）AOF 默认不开启，需要手动在配置文件中配置**

![img](./images/20210122125844358.png)

**3）可以在redis.conf中配置文件名称，默认为 appendonly.aof**

![img](./images/2021012212590222.png)



**4）AOF 和 RDB 同时开启，redis 听谁的**

- 系统默认取AOF的数据。

**5）AOF 文件故障备份**

AOF的备份机制和性能虽然和RDB不同, 但是备份和恢复的操作同RDB一样，都是拷贝备份文件，需要恢复时再拷贝到Redis工作目录下，启动系统即加载。

**6）AOF 文件故障恢复**
如遇到AOF文件损坏，可通过**redis-check-aof --fix appendonly.aof** 进行恢复

**7）AOF 同步频率设置**

- 始终同步，每次Redis的写入都会立刻记入日志。
- 每秒同步，每秒记入日志一次，如果宕机，本秒的数据可能丢失。
- 把不主动进行同步，把同步时机交给操作系统

**8）Rewrite**

AOF采用文件追加方式，文件会越来越大为避免出现此种情况，新增了重写机制,当AOF文件的大小超过所设定的阈值时，Redis就会启动AOF文件的内容压缩，只保留可以恢复数据的最小指令集.可以使用命令bgrewriteaof。

**9）Redis如何实现重写？**

AOF文件持续增长而过大时，会fork出一条新进程来将文件重写(也是先写临时文件最后再rename)，遍历新进程的内存中数据，每条记录有一条的Set语句。重写aof文件的操作，并没有读取旧的aof文件，而是将整个内存中的数据库内容用命令的方式重写了一个新的aof文件，这点和快照有点类似。

**10）何时重写？**

重写虽然可以节约大量磁盘空间，减少恢复时间。但是每次重写还是有一定的负担的，因此设定Redis要满足一定条件才会进行重写


![img](./images/20210122131019360.png)

系统载入时或者上次重写完毕时，Redis会记录此时AOF大小，设为base_size,如果Redis的AOF当前大小>= base_size +base_size*100% (默认)且当前大小>=64mb(默认)的情况下，Redis会对AOF进行重写。

**11）AOF 优点**

- 备份机制更稳健，丢失数据概率更低。
- 可读的日志文本，通过操作AOF稳健，可以处理误操作。

**12）AOF 缺点**

- 比起RDB占用更多的磁盘空间。
- 恢复备份速度要慢。
- 每次读写都同步的话，有一定的性能压力。
- 存在个别Bug，造成恢复不能。

### MySQL 什么时候适合创建索引，什么时候不适合创建索引？

**什么时候适合创建索引**

1）主键自动建立唯 一 索引

2）频繁作为查询条件的字段应该创建索引

3）查询中与其它表关联的字段，外键关系建立索引

4）频繁更新的字段不适合创建索引，因为每次更新不单是更新了记录还会更新索引

5）单键组索引的选择问题，who? 在高并发下领向创建组合索引

6）意询中排序的字段，排序字段若通过索引法访问将大大提高排序速度

7）查询中统计或者分组字段

**什么时候不适合创建索引**

1）表记录太少

2）经常增删改的表   因为更新表时，MySQL不仅要保存数据，还要保存一下索引文件数据重复且分布平均的表字段，因此应该只为最经常查询和最经常排序的数据列建立索引。

3）注意，如果某个数据列包含许多重复的内容，为它建立索弓|就没有太大的实际效果。

### JVM 垃圾回收机制，GC 发生在 JVM 哪部分，有几种 GC，他们的算法是什么？

![img](https://img2020.cnblogs.com/blog/644584/202004/644584-20200405223945708-1097995415.png)



**1、GC发生在JVM哪部分？**

　　GC是发生在堆内



**2、GC是什么？有几种GC？**

- GC是分代收集算法，在堆内不同的区域有不同的策略
- 有两种GC：Minor GC、Full GC

- 次数上频繁收集Young区　Minor GC
- 次数上较少收集Old区　Full GC
- 基本不动perm区(永久区）

**3、它们的算法是什么？**

　　GC4大算法：

- 引用计数法：只要有对象被引用，GC就不进行回收，这种方式已被淘汰（JVM的实现一般不采用这种方式，缺点：1）每次对对象赋值时均要维护引用计数器，且计数器本身也有一定的消耗；2）较难处理循环引用【A引用B，B引用A】）
- 复制算法（copying）：年轻代中使用的是Minor GC，这种GC算法采用的是复制算法（copying）：1）从根集合（GC Root）开始，通过Tracing从From中找到存活对象，拷贝到To中；2）From、To交换身份，下次内存分配从To开始；

![img](./images/644584-20200405225618889-676953103.png)

- 标记算法（Mark-Sweep）：老年代一般是由标记清除或者是标记清除与标记整理的混合实现

  - 标记（mark）：从根集合开始扫描，对存活的对象进行标记。

  - 清除（Sweep）：扫描整个内存空间，回收未被标记的对象，使用free-list记录可以区域
  - 优点：不需要额外的空间，在同一块内存空间操作
  - 缺点：两次扫描，耗时严重；会产生内存碎片

![img](./images/644584-20200405230651252-1118084461.png)

- 标记压缩（Mark-Compact）：老年代一般是由标记清除或者是标记清除与标记整理的混合实现
  - 标记（Mark）：与标记清除一样。
  - 压缩（Compact）：再次扫描并往一端滑动存活对象（在整理压缩阶段，不在对标记的对象做回收，而是通过所有存活对象都向一端移动，然后直接清除边界以外的内存）
  - 优点：没有内存碎片
  - 缺点：需要移动对象的成本

![img](./images/644584-20200405231629519-937731288.png)

 



老年代Full GC有两种算法结合使用：标记-清除-压缩（Mark-Sweep-Compact）

原理：

1. Mark-Sweep和Mark-Compact结合
2. 和Mark-Sweep一致，当进行多次GC后才Compact

Full GC两种算法结合使用，先标记进行清除，清除多次并产生很多内存碎片之后，再做压缩

优点：减少移动对象成本

## 4. 项目面试题

### 1. Redis在项目中的使用场景

| 数据类型 | 使用场景                                                     |
| -------- | ------------------------------------------------------------ |
| String   | 比如说 ，我想知道什么时候封锁一个IP地址。Incrby命令          |
| Hash     | 存储用户信息【id，name，age】Hset(key,field,value)Hset(userKey,id,101)Hset(userKey,name,admin)Hset(userKey,age,23)----修改案例----Hget(userKey,id)Hset(userKey,id,102)为什么不使用String 类型来存储Set(userKey,用信息的字符串)Get(userKey)不建议使用String 类型 |
| List     | 实现最新消息的排行，还可以利用List的push命令，将任务存在list集合中，同时使用另一个命令，将任务从集合中取出[pop]。Redis—list数据类型来模拟消息队列。【电商中的秒杀就可以采用这种方式来完成一个秒杀活动】 |
| Set      | 特殊之处：可以自动排重。比如说微博中将每个人的好友存在集合(Set)中，这样求两个人的共通好友的操作。我们只需要求交集即可。 |
| Zset     | 以某一个条件为权重，进行排序。京东：商品详情的时候，都会有一个综合排名，还可以按照价格进行排名。 |

 ### 2. Elasticsearch 与 solr 的区别？

**1）背景**

- 他们都是基于 Lucene 搜索服务器基础上开发，一款优秀的，高性能的企业级搜索服务器，【是因为他们都是基于分词技术构建的倒排索引的方式进行查询】

**2）开发语言**

- Java

**3）诞生时间**

- Solr：2004年诞生
- ES：2010年诞生

**4）主要区别**

- 当实时建立索引的时候，solr 会产生 io 阻塞，而 es 不会，es 查询性能要高于 solr
- 在不断动态添加数据的时候，solr 的检索效率会变得低下，而 es 没有什么变化
- Solr 利用 zookeeper 进行分布式管理，而 es 自带有分布式系统的管理功能，Solr 一般都要部署到 web 服务器上，比如 tomcat，启动 tomcat 的时候需要配置 tomcat 和 solr 的 关联 【 Solr 的本质，是一个动态的 web项目】
- Solr支持更多格式的数据 【xml、json、csv 】等，而 es 仅仅支持 json 文件格式
- Solr 是传统搜索应用的有利解决方案，但是 es 更加适用于新兴的是是搜索应用,单纯的对已有的数据进行检索， solr 效率更好，高于 es
- Solr 官网提供的功能更多哦，而 es 本身更加注重于核心功能，高级功能都有第三方插件完成
  

### 3. 单点登录

- 单点登录: 一处登录多处使用！
- 前提：单点登录多使用在分布式系统中

![image-20220218210114423](./images/image-20220218210114423.png)



### 4. 购物车实现过程

![image-20220218210421451](./images/image-20220218210421451.png)

### 5. 消息队列在项目中的使用

背景： 在分布式系统中如何处理高并发的

由于在高并发的环境下，来不及同步处理用户发送的请求，则会导致请求发生阻塞，比如说，大量的 insert，update 之类的请求同时到达数据库 MySQL, 直接导致无数的行锁表锁，甚至会导致请求堆积过多，从而触发 too many connections ( 链接数太多 ) 错误，使用消息队列可以解决 【异步通信】

- 1）异步

![image-20220218210455585](./images/image-20220218210455585.png)

- 并行

![image-20220218210504743](./images/image-20220218210504743.png)

- 排队

![image-20220218210513381](./images/image-20220218210513381.png)

- 消息队列电商使用场景：

![image-20220218210521404](./images/image-20220218210521404.png)

**消息队列的弊端：**

​	消息的不确定性：延迟队列，轮询技术来解决该问题即可！

