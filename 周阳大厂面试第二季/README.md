## 介绍

> 来源Bilibili尚硅谷周阳老师大厂面试题第二季教程：[硅谷大厂面试第二季教程](https://www.bilibili.com/video/BV18b411M7xz)

## 目录

### 大厂面试第二季

- [1_JUC多线程及并发包](./1_JUC多线程及并发包)

- [2_JVM+GC解析](./2_JVM+GC解析)
